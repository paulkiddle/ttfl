
## Members

<dl>
<dt><a href="#ttfl">ttfl</a> ⇒ <code><a href="#tagged">tagged</a></code></dt>
<dd><p>Tag a template for later.
Returns a function that you can pass the real tag function to.
If any of the template values are ttfl functions, the tag will be applied to them too.
You can also call ttfl as a regular function - in which case nested ttfls will not be altered.</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#join">join(parts, joiner)</a> ⇒ <code><a href="#tagged">tagged</a></code></dt>
<dd><p>Join an array of parts into a single value</p>
</dd>
</dl>

## Typedefs

<dl>
<dt><a href="#tagged">tagged</a> : <code>function</code></dt>
<dd><p>A tagged template callback, ready to be used</p>
</dd>
</dl>

<a name="ttfl"></a>

## ttfl ⇒ [<code>tagged</code>](#tagged)
Tag a template for later.
Returns a function that you can pass the real tag function to.
If any of the template values are ttfl functions, the tag will be applied to them too.
You can also call ttfl as a regular function - in which case nested ttfls will not be altered.



| Param | Type | Description |
| --- | --- | --- |
| lits | <code>String</code> | Template literal |
| ...values | <code>any</code> | Tempalte values |

<a name="join"></a>

## join(parts, joiner) ⇒ [<code>tagged</code>](#tagged)
Join an array of parts into a single value



| Param | Type | Description |
| --- | --- | --- |
| parts | <code>Array</code> | Parts to join together |
| joiner | <code>\*</code> | Value to use to join the parts together |

<a name="tagged"></a>

## tagged : <code>function</code>
A tagged template callback, ready to be used



| Param | Type | Description |
| --- | --- | --- |
| tag | <code>function</code> | A template tagging function to pass the saved template to. |

