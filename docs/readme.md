# Template Tag For Later

Tag your templates but defer the choice of tag function until a later date.

Great for library authors who want to allow consumers to select their own tag functions.

```javascript
// horses.js
import html from 'ttfl';

const template = html`<marquee>${'I <3 horses, best of all the animals'}</marquee>`;

export default template;
```

```javascript
import html from 'escape-html-template-tag';
import horseTemplate from './horses.js';

console.log(html(horseTemplate).toString());
```

You can also nest the templates for extra fun:

```javascript
import sql from 'ttfl';

const s1 = sql`SELECT author_id, count(*) as total FROM books GROUP BY author_id`;
const s2 = sql`SELECT * FROM (${s1}) AS books LEFT JOIN authors ON(authors.id=books.author_id);`

import sqlTag from 'sql-template-tag';

export default s2(sqlTag);
```
