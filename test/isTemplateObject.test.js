import { jest } from '@jest/globals';

test('Uses default template if available', async ()=>{
	const def = Array.isTemplateObject = jest.fn();

	const itto = await import('../src/isTemplateObject.js');

	itto.default();

	expect(def).toHaveBeenCalled();
});
