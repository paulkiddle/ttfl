import lib, {toString, join} from '../src/index.js';
import { jest } from '@jest/globals';

test('Creates tag for later', ()=>{
	const tagged = lib`One, ${2}, three`;

	const tag = jest.fn();

	tagged(tag);

	expect(tag).toHaveBeenCalledWith(['One, ', ', three'], 2);
});

test('Has toString method', () => {
	const tagged = lib`One, ${2}`;

	expect(tagged(toString)).toMatch('One, 2');
});

test('Nests tags', ()=>{
	const nested = lib`Nested, ${1}`;

	const tagged = lib`Top ${nested}`;

	const tag = jest.fn(toString);

	tagged(tag);

	expect(tag).toHaveBeenCalledWith(['Nested, ', ''], 1);

	expect(tag).toHaveBeenCalledWith(['Top ', ''], 'Nested, 1');
});

test('Works with regular function calls', ()=>{
	const ob = Symbol('Object');

	const saved = lib(ob, 2, 3);

	const tag = jest.fn();

	saved(tag);

	expect(tag).toHaveBeenCalledWith(ob, 2, 3);
});

test('Joins empty array', ()=>{
	const tag = jest.fn();
	join([])(tag);

	expect(tag).toHaveBeenCalledWith(['']);
});

test('Joins single item', ()=>{
	const tag = jest.fn();
	join(['1'])(tag);

	expect(tag).toHaveBeenCalledWith(['',''], '1');
});

test('Joins multiple items', ()=>{
	const tag = jest.fn(toString);
	join(['1','2'], ',')(tag);

	expect(tag).toHaveBeenCalledWith(['','','',''], '1',',','2');

	join(['a','b','c'])(tag);
	expect(tag).toHaveBeenCalledWith(['','','',''], 'a','','b');

	expect(tag).toHaveBeenCalledWith(['','','',''], 'ab','','c');
});
