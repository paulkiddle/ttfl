# Template Tag For Later

Tag your templates but defer the choice of tag function until a later date.

Great for library authors who want to allow consumers to select their own tag functions.

```javascript
// horses.js
import html from 'ttfl';

const template = html`<marquee>${'I <3 horses, best of all the animals'}</marquee>`;

export default template;
```

```javascript
import html from 'escape-html-template-tag';
import horseTemplate from './horses.js';

console.log(html(horseTemplate).toString());
```

You can also nest the templates for extra fun:

```javascript
import sql from 'ttfl';

const s1 = sql`SELECT author_id, count(*) as total FROM books GROUP BY author_id`;
const s2 = sql`SELECT * FROM (${s1}) AS books LEFT JOIN authors ON(authors.id=books.author_id);`

import sqlTag from 'sql-template-tag';

export default s2(sqlTag);
```

## Members

<dl>
<dt><a href="#ttfl">ttfl</a> ⇒ <code><a href="#tagged">tagged</a></code></dt>
<dd><p>Tag a template for later.
Returns a function that you can pass the real tag function to.
If any of the template values are ttfl functions, the tag will be applied to them too.
You can also call ttfl as a regular function - in which case nested ttfls will not be altered.</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#join">join(parts, joiner)</a> ⇒ <code><a href="#tagged">tagged</a></code></dt>
<dd><p>Join an array of parts into a single value</p>
</dd>
</dl>

## Typedefs

<dl>
<dt><a href="#tagged">tagged</a> : <code>function</code></dt>
<dd><p>A tagged template callback, ready to be used</p>
</dd>
</dl>

<a name="ttfl"></a>

## ttfl ⇒ [<code>tagged</code>](#tagged)
Tag a template for later.
Returns a function that you can pass the real tag function to.
If any of the template values are ttfl functions, the tag will be applied to them too.
You can also call ttfl as a regular function - in which case nested ttfls will not be altered.



| Param | Type | Description |
| --- | --- | --- |
| lits | <code>String</code> | Template literal |
| ...values | <code>any</code> | Tempalte values |

<a name="join"></a>

## join(parts, joiner) ⇒ [<code>tagged</code>](#tagged)
Join an array of parts into a single value



| Param | Type | Description |
| --- | --- | --- |
| parts | <code>Array</code> | Parts to join together |
| joiner | <code>\*</code> | Value to use to join the parts together |

<a name="tagged"></a>

## tagged : <code>function</code>
A tagged template callback, ready to be used



| Param | Type | Description |
| --- | --- | --- |
| tag | <code>function</code> | A template tagging function to pass the saved template to. |

