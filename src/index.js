import isTemplateObject from './isTemplateObject.js';

export const isTtfl = Symbol.for('isTtfl');

const flatten = tag => function flatten(value) {
	if(value && value[isTtfl]) {
		return value(tag);
	}

	return value;
};

function mapNested(tag, values){
	return values.map(flatten(tag));
}

/**
 * Tag a template for later.
 * Returns a function that you can pass the real tag function to.
 * If any of the template values are ttfl functions, the tag will be applied to them too.
 * You can also call ttfl as a regular function - in which case nested ttfls will not be altered.
 * @name ttfl
 * @param {String} lits Template literal
 * @param  {...any} values Tempalte values
 * @returns {tagged}
 */
export default function ttfl(lits, ...values) {
	const tagged = tag => {
		if(isTemplateObject(lits)) {
			values = mapNested(tag, values);
		}
		return tag(lits, ...values);
	};

	tagged[isTtfl] = true;

	return tagged;
}

export const toString = (lits, ...values) => lits.reduce((s, l, ix)=>s+String(values[ix-1])+l);

/**
 * Join an array of parts into a single value
 * @param {Array} parts Parts to join together
 * @param {*} joiner Value to use to join the parts together
 * @returns {tagged}
 */
export function join(parts, joiner=ttfl``) {
	if(parts.length===0){
		return ttfl``;
	}
	if(parts.length===1){
		return ttfl`${parts[0]}`;
	}
	return parts.reduce((part1, part2)=>ttfl`${part1}${joiner}${part2}`);
}

/**
 * A tagged template callback, ready to be used
 * @callback tagged
 * @param {Function} tag A template tagging function to pass the saved template to.
 */
