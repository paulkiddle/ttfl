
function isFrozenStringArray (array) {
	if(
		!Array.isArray(array) ||
		!Object.isFrozen(array) ||
		(array.length < 1))
		return false;

	return array.every(el=>typeof el === 'string');
}

function hasRaw(array) {
	const len = array && array.raw && array.raw.length;
	return len === array.length;
}

const isTemplateObject = Array.isTemplateObject ?
	o=>Array.isTemplateObject(o) :
	o=>hasRaw(o)&&isFrozenStringArray(o);

export default isTemplateObject;
